﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCrm
{
    /// <summary>
    /// Class of first kind of reports
    /// </summary>
    class FisrtReport : Report
    {
        public FisrtReport()
        {
            date = DateTime.Now;
            subject = "First kind of report";
            formatVersion = 1;
        }
    }
}
