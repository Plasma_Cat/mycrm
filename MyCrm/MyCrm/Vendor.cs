﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCrm
{
    /// <summary>
    /// Class of vendor
    /// </summary>
    class Vendor
    {
        public int id;
        public string name;
    }
}
