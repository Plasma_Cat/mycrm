﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCrm
{
    /// <summary>
    /// Class for operations with excel files
    /// </summary>
    class TableReader
    {


        /// <summary>
        /// Read a table
        /// </summary>
        /// <param name="filePath">Path to file</param>
        /// <returns>One of report of any kind</returns>
        public Report Read(string filePath)
        {
            var formatVersion = 1;//read format version at first and select a report class

            switch (formatVersion)
            {
                case 1:
                    var oneReport = new FisrtReport();
                    
                    //Read a table in format version 1

                    return oneReport;
                default:
                    break;
            }
            return null;
        }
    }
}
