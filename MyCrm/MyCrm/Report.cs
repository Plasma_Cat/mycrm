﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCrm
{
    /// <summary>
    /// essential of report
    /// </summary>
    abstract class Report
    {
        public int id;
        public DateTime date;
        public int formatVersion;
        public string subject;
    }
}
